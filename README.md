# 微博抖音小红书-小窗模式

#### 介绍
适用于PC浏览器的插件（扩展），一键打开微博/抖音/小红书及任意其他任意网页的小窗模式，针对小窗下的显示做了特别优化，并且可以一键自动隐藏窗口。  
![输入图片说明](%E5%AE%A3%E4%BC%A0%E5%9B%BE1.jpg)
![输入图片说明](%E5%8F%B3%E9%94%AE%E8%8F%9C%E5%8D%95.jpg)


#### 交流反馈群
 
QQ群：710979714  
QQ频道：https://pd.qq.com/s/fuzp4pfiu  
邮箱：  abudulalonghorn@gmail.com 

#### 安装/更新方式

##### Chrome 及其他采用采用 Chromium 内核的国产浏览器（如360、QQ、百度等）

前往官方商店 Chrome Web Store 一键安装（需翻墙）：  
https://chromewebstore.google.com/detail/ijkdallplaejdkiakdjdllmbghhoflfo  

##### Edge 浏览器

前往Edge 官方插件中心一键安装：  
https://microsoftedge.microsoft.com/addons/detail/pnhdclhmacafomaifhlndepbaiaopbpa


##### 从 gitee 或者网盘手动下载安装

gitee:  https://gitee.com/abudula7/weibodouyinxiaohongshu/releases  
百度网盘： https://pan.baidu.com/s/1_B7Awk0Js-cNL3DzJr3GPQ?pwd=txgk 提取码: txgk  
阿里云盘： https://www.alipan.com/s/MNwybT88ijU

##### 手动安装说明
1. 从网盘下载.crx文件（如 "微博抖音小红书-小窗模式_0_3_1.crx"）
2. 打开浏览器输入网址 chrome://extensions 或者 edge://extensions (或者点击右上角设置 -> 扩展程序 -> 管理扩展程序)  
3. 打开右上角开发者模式，并将下载的 .crx 拖入窗口即可
![输入图片说明](%E6%9C%AC%E5%9C%B0%E5%AE%89%E8%A3%85%E8%B7%AF%E5%BE%84-%E6%89%93%E5%BC%80%E5%BC%80%E5%8F%91%E8%80%85%E6%A8%A1%E5%BC%8F.png)  
4. （可选）固定到工具栏使用更方便快捷  
![输入图片说明](%E5%9B%BA%E5%AE%9A%E5%88%B0%E5%B7%A5%E5%85%B7%E6%A0%8F.jpg)